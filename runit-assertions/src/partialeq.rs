use crate::Assertion;

#[cfg(any(not(feature = "ansi_term"), not(feature = "dissimilar")))]
fn diff_report<'s, A, B>(expected: &'s A, value: &'s B) -> String
where
    A: std::fmt::Debug,
    B: std::fmt::Debug,
{
    format!("expected: {expected:?}\n     was: {value:?}")
}

#[cfg(all(feature = "ansi_term", feature = "dissimilar"))]
fn diff_report<'s, A, B>(expected: &'s A, value: &'s B) -> String
where
    A: std::fmt::Debug,
    B: std::fmt::Debug,
{
    let mut e_chunks = Vec::new();
    let mut v_chunks = Vec::new();
    let expected = format!("{expected:?}");
    let value = format!("{value:?}");
    for chunk in dissimilar::diff(&expected, &value) {
        use dissimilar::Chunk::{Delete, Equal, Insert};
        match chunk {
            Equal(s) => {
                e_chunks.push(ansi_term::Style::default().paint(s));
                v_chunks.push(ansi_term::Style::default().paint(s));
            }
            Delete(s) => {
                e_chunks.push(
                    ansi_term::Style::default()
                        .on(ansi_term::Colour::RGB(u8::MAX / 4, 0, 0))
                        .fg(ansi_term::Colour::Red)
                        .bold()
                        .paint(s),
                );
            }
            Insert(s) => {
                v_chunks.push(
                    ansi_term::Style::default()
                        .on(ansi_term::Colour::RGB(0, u8::MAX / 4, 0))
                        .fg(ansi_term::Colour::Green)
                        .bold()
                        .paint(s),
                );
            }
        }
    }
    let mut ansi_str = vec![ansi_term::Style::default().paint("expected: ")];
    ansi_str.extend(e_chunks);
    ansi_str.push(ansi_term::Style::default().paint("\n     was: "));
    ansi_str.extend(v_chunks);
    ansi_term::ANSIStrings(&ansi_str).to_string()
}

impl<'v, V> Assertion<'v, V>
where
    V: std::fmt::Debug,
{
    /// Assert if `self` is equal to an expected value.
    ///
    /// ```
    /// # use runit_assertions::assert_that;
    /// assert_that!(1 + 1).is_equal_to(2);
    /// assert_that!("foobar").is_equal_to("foobar");
    /// let foobar = String::from("foobar");
    /// assert_that!(foobar).is_equal_to("foobar");   // String == &str
    /// assert_that!("foobar").is_equal_to("foobar"); // &str == &str
    /// assert_that!("foobar").is_equal_to(&foobar);  // &str == &String
    /// assert_that!("foobar").is_equal_to(foobar);   // &str == String
    /// ```
    pub fn is_equal_to<C>(mut self, compared: C) -> Self
    where
        V: PartialEq<C> + std::fmt::Debug,
        C: std::fmt::Debug,
    {
        let value: &'v V = self.value;
        let pass = value.eq(&compared);
        let reporter = crate::Report {
            pass,
            condition: format!("should be equal to `{compared:?}`"),
            details: diff_report(&compared, &value),
        };
        self.reports.push(reporter);
        self
    }

    /// Assert if `self` is **not** equal to an expected value.
    ///
    /// ```
    /// # use runit_assertions::assert_that;
    /// assert_that!(1 + 1).is_not_equal_to(1);
    /// ```
    pub fn is_not_equal_to<C>(mut self, compared: C) -> Self
    where
        V: PartialEq<C> + std::fmt::Debug,
        C: std::fmt::Debug,
    {
        let value: &V = self.value;
        let pass = value.ne(&compared);
        let reporter = crate::Report {
            pass,
            condition: format!("should not be equal to `{compared:?}`"),
            details: format!("was: {value:?}"),
        };
        self.reports.push(reporter);
        self
    }
}
