#![warn(
    missing_copy_implementations,
    missing_debug_implementations,
    missing_docs,
    rust_2018_idioms
)]
#![recursion_limit = "256"]
//! An assertion library with a fluent API for Rust

mod partialeq;
#[cfg(feature = "regex")]
mod regex;

/// Use it to start an assertion chain in tests.
///
/// ```
/// # use runit_assertions::assert_that;
/// assert_that!(1 + 1).is_equal_to(2);
/// ```
#[macro_export]
macro_rules! assert_that {
    ($value:expr) => {{
        runit_assertions::Assertion::from(&($value))
    }};
}

#[derive(Debug)]
struct Report {
    pass: bool,
    condition: String,
    details: String,
}

impl Report {}

impl std::fmt::Display for Report {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let emoji = if self.pass { '✓' } else { '⨯' }.to_string();
        #[cfg(not(feature = "ansi_term"))]
        let report = format!(
            "\n\n{emoji} {}\n{}",
            self.condition.trim(),
            self.details.trim()
        );
        #[cfg(feature = "ansi_term")]
        let report = {
            let style = if self.pass {
                ansi_term::Colour::Green.normal()
            } else {
                ansi_term::Colour::Red.bold()
            };
            let mut chunks = vec![
                ansi_term::Style::default().paint("\n"),
                style.paint(emoji),
                style.paint(" "),
                style.paint(self.condition.trim()),
            ];
            if !self.pass {
                chunks.push(ansi_term::Style::default().paint("\n"));
                chunks.push(ansi_term::Style::default().paint(self.details.trim()));
            }
            ansi_term::ANSIStrings(&chunks).to_string()
        };
        write!(f, "{report}")
    }
}

/// `Assertion` is the starting point for a chain of assertions. You
/// usually create these using the macro [`assert_that`].
///
/// [`assert_that`]: ./macro.assert_that.html
#[derive(Debug)]
pub struct Assertion<'v, V>
where
    V: std::fmt::Debug,
{
    value: &'v V,
    location: &'static std::panic::Location<'static>,
    reports: Vec<Report>,
}

impl<'v, V> From<&'v V> for Assertion<'v, V>
where
    V: 'v + std::fmt::Debug,
{
    #[track_caller]
    fn from(value: &'v V) -> Self {
        Assertion {
            value,
            location: std::panic::Location::caller(),
            reports: Vec::new(),
        }
    }
}

impl<V> std::ops::Drop for Assertion<'_, V>
where
    V: std::fmt::Debug,
{
    fn drop(&mut self) {
        let nb_fails = self.reports.iter().filter(|r| !r.pass).count();
        let location = format!("{}:{}:", self.location.file(), self.location.line());
        #[cfg(not(feature = "ansi_term"))]
        let location = format!("\n{location}");
        #[cfg(feature = "ansi_term")]
        let location = ansi_term::ANSIStrings(&[
            ansi_term::Style::default().paint("\n"),
            ansi_term::Colour::Yellow.bold().paint(location),
        ])
        .to_string();
        let message = self
            .reports
            .iter()
            .map(ToString::to_string)
            .fold(location, |msg, r| msg + &r);
        assert!(nb_fails == 0, "{}\n\n", message);
    }
}
