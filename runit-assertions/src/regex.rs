use crate::Assertion;

#[derive(Debug)]
pub struct Regex(regex::Regex);

impl From<regex::Regex> for Regex {
    fn from(regex: regex::Regex) -> Self {
        Self(regex)
    }
}
impl From<&regex::Regex> for Regex {
    fn from(regex: &regex::Regex) -> Self {
        Self(regex.clone())
    }
}
impl From<&str> for Regex {
    fn from(s: &str) -> Self {
        let regex = regex::Regex::new(s)
            .unwrap_or_else(|e| panic!("{s} is not a valid regular expression: {e}"));
        Regex(regex)
    }
}
impl From<String> for Regex {
    fn from(s: String) -> Self {
        Regex::from(s.as_str())
    }
}
impl From<&String> for Regex {
    fn from(s: &String) -> Self {
        Regex::from(s.as_str())
    }
}
impl From<char> for Regex {
    fn from(c: char) -> Self {
        Regex::from(c.to_string())
    }
}

impl<'v, V> Assertion<'v, V>
where
    V: std::fmt::Debug + AsRef<str>,
{
    /// Assert if `self` matches a regular expression.
    ///
    /// Note that [`regex`] crate is used for regular expression processing.
    ///
    /// ```
    /// # use runit_assertions::assert_that;
    /// assert_that!("foobar").matches(regex::Regex::new("(foo|bar)+").unwrap());
    /// assert_that!("foobar").matches("(foo|bar)+");
    /// assert_that!("💖💔💝💘").matches('💘');
    /// ```
    pub fn matches(mut self, regex: impl Into<Regex>) -> Self
    where
        V: std::fmt::Debug,
    {
        let regex = regex.into().0;
        let value: &str = self.value.as_ref();
        let pass = regex.is_match(value);
        let reporter = crate::Report {
            pass,
            condition: format!("should match `{regex:?}`"),
            details: format!("was: {:?}", self.value),
        };
        self.reports.push(reporter);
        self
    }
}
